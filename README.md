# Configuring SSH for GitLab/GitHub in Windows 10

This document shows how to set up and use SSH keys for GitLab/GitHub in Windows 10. You can use the same SSH key for GitLab and GitHub if you want. 

https://gitlab.com/help/ssh/README 

## To check if you already have an existing SSH key file
- Open a **command prompt**
- `type %userprofile%\.ssh\id_rsa.pub`

### To Generate a new SSH key
- Open **Git Bash**
- `ssh-keygen -t rsa -C "yourgitlogin@mail.com"`
- set the filename and passphrase
- to change the passphrase later, use `ssh-keygen -p`

### Deploy the SSH key
- Open the *.pub file with Notepad++
- Select all, copy
- Paste into 
	- GitLab: https://gitlab.com/profile/keys 
	- GitHub: https://github.com/settings/keys

### Setting up ssh-agent
To actually use the keys, you need to register them with ssh-agent. Otherwise, you will have to enter your passphrase on every push/pull, and that's no fun! It's a bit tricky to set it up under Windows, and this is the best way to do it I found so far:

- Download the latest release of **SSH-agent-helper**: https://github.com/raeesbhatti/ssh-agent-helper/releases
- Put it into 'C:\Tools`
- Make sure your path includes `C:\Program Files\Git\usr\bin` / `C:\Users\[USERNAME]\AppData\Local\Programs\Git\usr\bin` (ssh-agent.exe should be there). **Reboot** if you need to change this! Really!
- Open a command prompt at C:\Tools
- Execute `ssh -T git@github.com`, type **yes** when requested
- `ssh-agent-helper --help` displays the options
- To start it up with windows and add the SSH key created above, use  
`ssh-agent-helper -r -a %USERPROFILE%\.ssh\id_rsa`.  
Now when Windows starts the ssh-agent will get started and ask you for the passphrase **once**. After entering the passphrase, you will not have to enter it again until you restart Windows again. 
- You can also create a simple .bat file with the following command:  
`echo @ssh-agent-helper -a %USERPROFILE%\.ssh\id_rsa > ssh-agent-helper-add.bat`  
Now you can use that file to add the key in case you dismissed the startup passphrase window

### Testing if it works
- Open **Git Bash**
- `ssh -T git@gitlab.com`
- `ssh -T git@github.com`

### Misc
- `eval $(ssh-agent -s)`
- `ssh-add ~/.ssh/id_rsa`
- command: winpty

### Updates 2017-05-05:
- Create a `.bashrc` file in your profile dir (type `cd` into your bash/cmd) by typing `touch .bashrc`
- Copy the code from here: https://superuser.com/a/230872/394961 into your `.bashrc` file:

```
echo "executing .bashrc"

set -o ignoreeof

SSH_ENV="$HOME/.ssh/environment"

function start_agent {
     echo "Initialising new SSH agent..."
     /usr/bin/ssh-agent | sed 's/^echo/#echo/' > "${SSH_ENV}"
     echo succeeded
     chmod 600 "${SSH_ENV}"
     . "${SSH_ENV}" > /dev/null
     /usr/bin/ssh-add;
}

# Source SSH settings, if applicable

if [ -f "${SSH_ENV}" ]; then
     . "${SSH_ENV}" > /dev/null
     #ps ${SSH_AGENT_PID} doesn't work under cywgin
     ps -efp ${SSH_AGENT_PID} | grep ssh-agent$ > /dev/null || {
         start_agent;
     }
else
     start_agent;
fi
```

- Open a git session, it should ask you for your passphrase, if not...
    - http://stackoverflow.com/a/32353083/54159: create a `.bash_profile` file in your profile dir, with this content:  

```
if [ -f ~/.bashrc ]; then . ~/.bashrc; fi
```

- Bonus: Windows+R, `shell:startup`, then add `Git Bash` here to get prompted on windows startup (don't forget to unregister ssh-agent-helper)
